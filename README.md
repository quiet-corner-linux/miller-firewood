# Miller Firewood

This project is for the Miller Firewood & Logging website. The app is built using Ruby on Rails. It is static, has no database, and consists of a single landing page.

## System Environment

* [Ruby](https://www.ruby-lang.org/en/) 3.2.2
* [Rails](https://rubyonrails.org/) 7.0.4.3
* [Tailwind](https://tailwindcss.com/)
* [Slim](https://slim-template.github.io/)

## Deployment

Respective branches will be pushed to [Railway](https://railway.app/). See [.gitlab-ci.yml](/.gitlab-ci.yml)

### Environments

- Staging
	- branch: staging
	- url: https://miller-firewood-staging.up.railway.app/
- Production
	- branch: main
  - url: https://www.millerfirewood.com/

## To Do

- [ ] Finish content for environment and wildfile sections.
- [ ] Update images for 2023.
- [ ] Add variety to content presentation.
