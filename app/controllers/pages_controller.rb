class PagesController < ApplicationController
	def show
		if valid_page?
			add_seo
			render template: "pages/#{params[:page]}"
		else
			render file: "public/404.html", status: :not_found
		end
	end

	private
	def valid_page?
		File.exist?(Pathname.new(Rails.root + "app/views/pages/#{params[:page]}.html.slim"))
	end

	def add_seo
		case params[:page]
			when "about"
				@page_title = "About"
				@page_description = "Miller Firewood &amp; Logging serves Rhode Island with delivered firewood, cut and split or log length."
				@page_keywords = [
					"firewood near me",
					"firewood for sale",
					"firewood delivery near me",
					"cord of firewood",
					"where to buy firewood",
					"firewood in ri",
					"ri",
					"rhode island",
					"firewood", 
					"logging",
				]
		end
	end
end
